"""
This is the main file for hello package.
"""

lang="en"

def main(lang="en"):
    """This is the main function."""
    if lang == "es":
       spanish()

    return 0

def spanish():
    """This is the english version."""
    print("Buenos dias!")

def english():
    """This is the english version."""
    print("Hello!")

